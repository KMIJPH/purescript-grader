# grader

## Description

Converts points obtained in a test to grades.  
A running example can be found [here](https://kmijph.codeberg.page/apps/grader/).

## Usage

- `Lower` and `Upper` control the slope of the conversion
- `Floor` specifies the lowest grade possible
- `Ceil` specifies the highest grade possible
- `Max Points` specifies the maximum number of points obtainable
- `Percent` specifies the percent of points needed to obtain a positive grade
- `Positive` specifies the point in the grading scaling at which a grade is positive
- `Linear` switches between linear and nonlinear mode

Takes a (`,` separated) `.csv` file as an input that HAS to contain a `name` and a `p` (points) field ([example](./etc/test.csv)).  
An output `.csv` file can be generated which will include the calculated grades.

# Building

```bash
spago bundle
```
