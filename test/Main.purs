-- File Name: Main.purs
-- Description: Grader tests
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 16 Mar 2023 07:24:24
-- Last Modified: 02 May 2023 23:11:58

module Test.Main where

import Prelude

import Grader.Calc (linear, nonlinear, grade2str, sepNumber, graphLinear, graphNonLinear, numRange, generateLinear, round2dec)
import Grader.Csv (toRows, findCol, parseRow, parseCsv, toCsv)
import Data.Array (head)
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Test.Unit (test)
import Test.Unit.Assert (equal)
import Test.Unit.Main (runTest)

-- csv string with whitespaces
csv1 :: String
csv1 =
  """name,p
Anton    ,10.2  
Rudi     ,12.5
Siegfried,15.2"""

csv2 :: String
csv2 =
  """name,p,g_num,g_str
Anton    ,10.2  ,12.0  ,33
Rudi     ,12.5  ,13.4  ,12
Siegfried,15.2"""

main :: Effect Unit
main = runTest do
  test "round2dec" do
    equal (round2dec 12.123123213) 12.12
    equal (round2dec 33.273123213) 33.27
    equal (round2dec 33.278123213) 33.28
    equal (round2dec 33.018123213) 33.02
    equal (round2dec 1202.4446) 1202.44
    equal (round2dec 1202.4456) 1202.45
  test "sepNumber" do
    equal (sepNumber 4.2) (Tuple 4 20)
    equal (sepNumber 22.253) (Tuple 22 25)
    equal (sepNumber 22.256) (Tuple 22 26)
    equal (sepNumber 0.12) (Tuple 0 12)
    equal (sepNumber 0.124) (Tuple 0 12)
  test "grade2str" do
    equal (grade2str 4.2) "4"
    equal (grade2str 4.25) "4+"
    equal (grade2str 6.6) "6/7"
    equal (grade2str 6.75) "7-"
    equal (grade2str 9.99) "10-"
    equal (grade2str 10.0) "10"
  test "numRange" do
    equal (numRange 0.0 10.0 11)
      [ 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 ]
    equal (numRange 0.0 (-20.0) 11)
      [ -20.0, -18.0, -16.0, -14.0, -12.0, -10.0, -8.0, -6.0, -4.0, -2.0, 0.0 ]
  test "graphLinear" do
    equal (graphLinear opts)
      ( Tuple [ 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0 ]
          [ 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.1499999999999995, 4.6, 5.05, 5.5, 5.95, 6.4, 6.85, 7.3, 7.75, 8.2, 8.65, 9.1, 9.55, 10.0 ]
      )

  test "graphNonLinear" do
    equal (graphNonLinear opts)
      ( Tuple [ 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0 ]
          [ 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.5, 5.0, 5.5, 6.0, 6.4, 6.8, 7.199999999999999, 7.6, 8.0, 8.4, 8.8, 9.2, 9.6, 10.0 ]
      )
  test "linear" do
    --                   x   min   max low high max_points
    equal (linear 2.0 1.0 10.0 4.0 10.0 20.0) 4.0
    equal (linear 9.0 1.0 10.0 4.0 10.0 20.0) 5.05
    equal (linear 10.0 1.0 10.0 4.0 10.0 20.0) 5.5
    equal (linear 11.0 1.0 10.0 4.0 10.0 20.0) 5.95
    equal (linear 11.5 1.0 10.0 4.0 10.0 20.0) 6.175
    equal (linear 19.0 1.0 10.0 4.0 10.0 20.0) 9.55
    equal (linear 21.0 1.0 10.0 4.0 10.0 20.0) 10.0
    equal (linear 5.0 1.0 10.0 4.0 10.0 10.0) 5.5
    equal (linear 15.0 1.0 10.0 4.0 10.0 30.0) 5.5
  test "nonlinear" do
    --                   x   min   max low high max_points
    equal (nonlinear 2.0 1.0 10.0 4.0 10.0 20.0 opts.perc opts.pos) 4.0
    equal (nonlinear 9.0 1.0 10.0 4.0 10.0 20.0 opts.perc opts.pos) 5.5
    equal (nonlinear 10.0 1.0 10.0 4.0 10.0 20.0 opts.perc opts.pos) 6.0
    equal (nonlinear 11.0 1.0 10.0 4.0 10.0 20.0 opts.perc opts.pos) 6.4
    equal (nonlinear 11.5 1.0 10.0 4.0 10.0 20.0 opts.perc opts.pos) 6.6
    equal (nonlinear 19.0 1.0 10.0 4.0 10.0 20.0 opts.perc opts.pos) 9.6
    equal (nonlinear 21.0 1.0 10.0 4.0 10.0 20.0 opts.perc opts.pos) 10.0
    equal (nonlinear 5.0 1.0 10.0 4.0 10.0 10.0 opts.perc opts.pos) 6.0
    equal (nonlinear 15.0 1.0 10.0 4.0 10.0 30.0 opts.perc opts.pos) 6.0
  test "toRows" do
    equal (toRows csv1)
      [ [ "name", "p" ]
      , [ "Anton", "10.2" ]
      , [ "Rudi", "12.5" ]
      , [ "Siegfried", "15.2" ]
      ]
    equal (toRows csv2)
      [ [ "name", "p", "g_num", "g_str" ]
      , [ "Anton", "10.2", "12.0", "33" ]
      , [ "Rudi", "12.5", "13.4", "12" ]
      , [ "Siegfried", "15.2" ]
      ]
  test "findCol" do
    equal (findCol h "name") 0
    equal (findCol h "p") 1
    equal (findCol h "something") (-1)
  test "parseRow" do
    equal (parseRow [ "name", "p" ] 0 1) (Nothing)
    equal (parseRow [ "Samuel", "10.3" ] 0 1) (Just { name: "Samuel", p: 10.3 })
    equal (parseRow [ "Samuel", "10.3" ] 2 4) (Nothing)
    equal (parseRow [ "Oswaldo", " 0.4" ] 0 1) (Just { name: "Oswaldo", p: 0.4 })
  test "parseCsv" do
    equal (parseCsv csv1) test_arr
    equal (parseCsv csv2) test_arr
  test "toCsv" do
    equal (toCsv $ generateLinear test_arr opts) res
  where
  opts =
    { lower: 1.0
    , upper: 10.0
    , floor: 4.0
    , ceil: 10.0
    , max_p: 20.0
    , perc: 50.0
    , pos: 6.0
    , lin: true
    }
  res = "name,p,g_num,g_str\nAnton,10.2,5.59,5/6\nRudi,12.5,6.625,6/7\nSiegfried,15.2,7.84,8-"
  test_arr =
    [ { name: "Anton", p: 10.2 }
    , { name: "Rudi", p: 12.5 }
    , { name: "Siegfried", p: 15.2 }
    ]
  rows = toRows csv1
  h = case head rows of
    Nothing -> []
    Just r -> r
