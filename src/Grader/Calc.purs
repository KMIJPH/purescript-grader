-- File Name: Calc.purs
-- Description: Grader logic
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 16 Mar 2023 07:24:24
-- Last Modified: 02 May 2023 23:11:19

module Grader.Calc where

import Prelude

import Data.Array (replicate, range, zipWith)
import Data.Int (floor, toNumber)
import Data.Number.Format (toString)
import Data.Number (round)
import Data.Tuple (Tuple(..), fst, snd)
import Grader.Csv (CsvIn, CsvOut)

-- | calculation options
type Options =
  { lower :: Number
  , upper :: Number
  , floor :: Number
  , ceil :: Number
  , max_p :: Number
  , perc :: Number
  , pos :: Number
  , lin :: Boolean
  }

-- | round to 2 decimal places
round2dec :: Number -> Number
round2dec num = (round (num * 100.0)) / 100.0

-- | creates an array of 'length' 'from' - 'to' incremented by equally sized steps
numRange :: Number -> Number -> Int -> Array Number
numRange from to length =
  zipWith (*) arr index
  where
  step = (to - from) / toNumber (length - 1)
  arr = replicate length step
  index
    | from < to = range 0 length # map (\e -> toNumber e)
    | otherwise = range (length - 1) 0 # map (\e -> toNumber e)

-- | separates a float into its integer and decimal parts
sepNumber :: Number -> Tuple Int Int
sepNumber x =
  Tuple i d
  where
  i = floor x
  d = floor $ round $ (x - toNumber i) * 100.0

-- | converts a separated grade to the corresponding string representation
grade2str :: Number -> String
grade2str g =
  out
  where
  sep = sepNumber $ g
  i = toNumber $ fst sep
  d = snd sep
  out
    | d >= 75 = (toString $ i + 1.0) <> "-"
    | d >= 50 = (toString $ i) <> "/" <> (toString $ i + 1.0)
    | d >= 25 = (toString $ i) <> "+"
    | otherwise = toString $ i

-- | normalizes a value from an old range (min max) to a new range(min' max')
normalize :: Number -> Number -> Number -> Number -> Number -> Number
normalize min max min' max' x = (max' - min') / (max - min) * (x - max) + max'

-- | restricts a value to a range
restrict :: Number -> Number -> Number -> Number
restrict x min max
  | x < min = min
  | x > max = max
  | otherwise = x

-- | returns a grade according to the points obtained by a student
-- it is a linear relationship, where the grading can be controlled via the 'min' and 'max' parameters
-- which will shift the amount of points needed to get a positive score
-- the lowest grade can be controlled via the 'lowest' parameter
-- the highest grade can be controlled via the 'highest' parameter
linear :: Number -> Number -> Number -> Number -> Number -> Number -> Number
linear x min max lowest highest max_points =
  restrict y lowest highest
  where
  y = normalize 0.0 max_points min max x

-- | returns a grade according to the points obtained by a student
-- non-linear version, where the percent of points needed to obtain a positive grade controls the split point
nonlinear :: Number -> Number -> Number -> Number -> Number -> Number -> Number -> Number -> Number
nonlinear x min max lowest highest max_points percent positive =
  restrict y lowest highest
  where
  needed = max_points * (percent / 100.0)
  y
    | x < needed = normalize 0.0 needed min positive x
    | otherwise = normalize needed max_points positive max x

-- | returns the linear grading scale as x,y data
graphLinear :: Options -> Tuple (Array Number) (Array Number)
graphLinear opts =
  Tuple x y
  where
  x = numRange 0.0 opts.max_p (floor opts.max_p + 1)
  y = map (\e -> linear e opts.lower opts.upper opts.floor opts.ceil opts.max_p) x

-- | returns the nonlinear grading scale as x,y data
graphNonLinear :: Options -> Tuple (Array Number) (Array Number)
graphNonLinear opts =
  Tuple x y
  where
  x = numRange 0.0 opts.max_p (floor opts.max_p + 1)
  y = map (\e -> nonlinear e opts.lower opts.upper opts.floor opts.ceil opts.max_p opts.perc opts.pos) x

-- | generates the output
generateLinear :: Array CsvIn -> Options -> Array CsvOut
generateLinear xs opts =
  xs
    # map \e ->
        { name: e.name
        , p: e.p
        , g_num: linear e.p opts.lower opts.upper opts.floor opts.ceil opts.max_p
        , g_str: grade2str $ linear e.p opts.lower opts.upper opts.floor opts.ceil opts.max_p
        }

-- | generates the output
generateNonLinear :: Array CsvIn -> Options -> Array CsvOut
generateNonLinear xs opts =
  xs
    # map \e ->
        { name: e.name
        , p: e.p
        , g_num: nonlinear e.p opts.lower opts.upper opts.floor opts.ceil opts.max_p opts.perc opts.pos
        , g_str: grade2str $ nonlinear e.p opts.lower opts.upper opts.floor opts.ceil opts.max_p opts.perc opts.pos
        }
