-- File Name: Csv.purs
-- Description: Simple csv parser for the input format
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 15 Mar 2023 17:02:26
-- Last Modified: 02 May 2023 23:11:25

module Grader.Csv where

import Prelude

import Data.Maybe (Maybe(..))
import Data.Number (fromString)
import Data.Number.Format (toString)
import Data.String (Pattern(..), split, contains, trim, joinWith)
import Data.Array (catMaybes, findIndex, index, head, tail)
import Data.Tuple (Tuple(..))

-- | csv input
type CsvIn =
  { name :: String
  , p :: Number
  }

-- | csv output
type CsvOut =
  { name :: String
  , p :: Number
  , g_num :: Number
  , g_str :: String
  }

-- | splits a string by a separator
splitStr :: String -> String -> Array String
splitStr str sep = (Pattern sep) `split` str

-- | converts the csv input to rows
toRows :: String -> Array (Array String)
toRows str =
  splitStr str "\n"
    # map \e -> splitStr e ","
        # map \el -> trim el

-- | returns column index in an array if it contains the column or -1 if it doesnt
findCol :: Array String -> String -> Int
findCol xs col =
  case i of
    Nothing -> -1
    Just index -> index
  where
  i = findIndex (contains $ Pattern col) xs

-- | overly complicated function to parse a row
parseRow :: Array String -> Int -> Int -> Maybe CsvIn
parseRow xs name_col point_col = case xs of
  [] -> Nothing
  [ _ ] -> Nothing
  _ -> case Tuple name points of
    (Tuple (Just n) (Just p)) -> case fromString p of
      Just num -> Just { name: n, p: num }
      Nothing -> Nothing
    _ -> Nothing
    where
    name = index xs name_col
    points = index xs point_col

-- | resolves a maybe for a row
unmaybeRow :: forall a. Maybe (Array a) -> Array a
unmaybeRow xs =
  case xs of
    Nothing -> []
    Just x -> x

-- | parses the csv file to valid records
parseCsv :: String -> Array CsvIn
parseCsv str =
  xt
    # map (\e -> parseRow e name_col point_col)
    # catMaybes
  where
  xs = toRows $ str
  xh = unmaybeRow $ head xs
  xt = unmaybeRow $ tail xs
  name_col = findCol xh "name"
  point_col = findCol xh "p"

-- | creates a csv string from the generated output
toCsv :: Array CsvOut -> String
toCsv xs =
  "name,p,g_num,g_str\n" <> str
  where
  str =
    xs
      # map (\e -> e.name <> "," <> toString e.p <> "," <> toString e.g_num <> "," <> e.g_str)
      # joinWith "\n"
