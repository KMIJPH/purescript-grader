-- File Name: Main.purs
-- Description: Simple tool to calculate grades from a csv file of students
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 15 Mar 2023 17:02:26
-- Last Modified: 02 May 2023 23:11:07

module Main where

import Prelude

import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Aff (launchAff_)
import Effect.Class (liftEffect)
import Effect.Exception (throw)
import UI (disableFields, ids, run, tableUpdate, graphUpdate, outputUpdate)
import UI.Table (getElem)
import Web.DOM.Element as E
import Web.Event.EventTarget (addEventListener, eventListener)
import Web.HTML (window)
import Web.HTML.Event.EventTypes (change, click)
import Web.HTML.HTMLDocument (toDocument)
import Web.HTML.Window (document)

main :: Effect Unit
main = do
  w <- window
  d <- document w
  let doc = toDocument d
  file_selector <- getElem doc ids.file_selector
  linear_checkbox <- getElem doc ids.linear_check
  download_button <- getElem doc ids.download_button
  lower_input <- getElem doc ids.lower_input
  upper_input <- getElem doc ids.upper_input
  floor_input <- getElem doc ids.floor_input
  ceil_input <- getElem doc ids.ceil_input
  max_input <- getElem doc ids.max_input
  percent_input <- getElem doc ids.percent_input
  positive_input <- getElem doc ids.positive_input

  launchAff_ $ run graphUpdate

  file_target <- case file_selector of
    Nothing -> throw $ "No element '" <> ids.file_selector <> "' found"
    Just b -> pure $ E.toEventTarget b

  linear_target <- case linear_checkbox of
    Nothing -> throw $ "No element '" <> ids.linear_check <> "' found"
    Just b -> pure $ E.toEventTarget b

  download_target <- case download_button of
    Nothing -> throw $ "No element '" <> ids.download_button <> "' found"
    Just b -> pure $ E.toEventTarget b

  lower_target <- case lower_input of
    Nothing -> throw $ "No element '" <> ids.lower_input <> "' found"
    Just b -> pure $ E.toEventTarget b

  upper_target <- case upper_input of
    Nothing -> throw $ "No element '" <> ids.upper_input <> "' found"
    Just b -> pure $ E.toEventTarget b

  floor_target <- case floor_input of
    Nothing -> throw $ "No element '" <> ids.floor_input <> "' found"
    Just b -> pure $ E.toEventTarget b

  ceil_target <- case ceil_input of
    Nothing -> throw $ "No element '" <> ids.ceil_input <> "' found"
    Just b -> pure $ E.toEventTarget b

  max_target <- case max_input of
    Nothing -> throw $ "No element '" <> ids.max_input <> "' found"
    Just b -> pure $ E.toEventTarget b

  percent_target <- case percent_input of
    Nothing -> throw $ "No element '" <> ids.percent_input <> "' found"
    Just b -> pure $ E.toEventTarget b

  positive_target <- case positive_input of
    Nothing -> throw $ "No element '" <> ids.positive_input <> "' found"
    Just b -> pure $ E.toEventTarget b

  file_listener <- eventListener \_ -> launchAff_ do
    run tableUpdate
  linear_listener <- eventListener \_ -> launchAff_ do
    liftEffect disableFields
    run graphUpdate
    run tableUpdate
  update_listener <- eventListener \_ -> launchAff_ do
    run graphUpdate
    run tableUpdate
  download_listener <- eventListener \_ -> launchAff_ do
    run outputUpdate

  addEventListener click download_listener false download_target
  addEventListener change file_listener false file_target
  addEventListener change linear_listener false linear_target
  addEventListener change update_listener false lower_target
  addEventListener change update_listener false upper_target
  addEventListener change update_listener false floor_target
  addEventListener change update_listener false ceil_target
  addEventListener change update_listener false max_target
  addEventListener change update_listener false percent_target
  addEventListener change update_listener false positive_target
