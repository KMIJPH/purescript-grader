"use strict";

export const _encodeURIComponent = encodeURIComponent;

// adds new chart data
function addData(chart, x, y, opts) {
    chart.options.scales.x.min = 0;
    chart.options.scales.x.max = x[-1];
    chart.options.scales.y.min = opts.floor;
    chart.options.scales.y.max = opts.ceil;

    chart.data.datasets.push({
        data: y,
        borderColor: "#3e95cd",
        fill: false
    });
    chart.data.labels = x;
    chart.update("none");
}

// removes chart data
function removeData(chart) {
    chart.data.datasets = [];
    chart.data.labels = [];
    chart.update("none");
}

// chart.js line chart
export const _chart = function(x) {
    return function(y) {
        return function(opts) {
            return function(canvas_id) {
                const chart = Chart.getChart(canvas_id);
                if (chart != undefined) {
                    removeData(chart);
                    addData(chart, x, y, opts);
                } else {
                    new Chart(canvas_id, {
                        type: "line",
                        data: {
                            labels: x,
                            datasets: [{
                                data: y,
                                borderColor: "#3e95cd",
                                fill: false
                            }]
                        },
                        options: {
                            plugins: {
                                legend: {
                                    display: false
                                },
                            },
                            scales: {
                                x: {
                                    min: 0,
                                    max: x[-1],
                                    title: {
                                        display: true,
                                        text: "Points"
                                    }
                                },
                                y: {
                                    min: opts.floor,
                                    max: opts.ceil,
                                    title: {
                                        display: true,
                                        text: "Grade"
                                    }
                                },
                            }
                        }
                    });
                }
            }
        }
    }
}
