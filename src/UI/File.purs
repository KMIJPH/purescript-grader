-- File Name: File.purs
-- Description: File IO
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 28 Mar 2023 23:18:25
-- Last Modified: 01 May 2023 12:01:42

module UI.File (readAsText, getFile) where

import Control.Monad.Except (runExcept)
import Data.Maybe (Maybe(..))
import Data.Either (Either(Left,Right), either)
import Foreign (F, Foreign, readString)
import Effect (Effect)
import Effect.Aff (makeAff, Aff)
import Effect.Exception (error)
import Prelude
import Web.Event.EventTarget (eventListener, addEventListener)
import Web.File.FileReader (FileReader, result, fileReader, toEventTarget)
import Web.File.FileReader as FileReader
import Web.File.FileList (item)
import Web.DOM.Internal.Types (Element)
import Web.File.File (File)
import Web.File.Blob (Blob)
import Web.HTML.Event.EventTypes as EventTypes
import Web.HTML.HTMLInputElement as I

-- | returns the first file from the fileList
getFile :: Maybe Element -> Effect (Maybe File)
getFile f = do
    case f of
        Nothing -> pure Nothing
        Just element -> do
            let input_element = I.fromElement element
            case input_element of
                Nothing -> pure Nothing
                Just input -> do
                    fl <- I.files input
                    case fl of
                        Nothing -> pure Nothing
                        Just fileList -> do
                            pure $ item 0 fileList

-- | https://github.com/nwolverson/purescript-dom-filereader
readAs :: forall a. (Foreign -> F a) -> (Blob -> FileReader -> Effect Unit) -> Blob -> Aff a
readAs readMethod getResult blob = makeAff \fun -> do
    let err = fun <<< Left
        succ = fun <<< Right
    fr <- fileReader
    let et = toEventTarget fr

    errorListener <- eventListener \_ -> err (error "error")
    loadListener <- eventListener \_ -> do
        res <- result fr
        either (\errs -> err $ error $ show errs) succ $ runExcept $ readMethod res

    addEventListener EventTypes.error errorListener false et
    addEventListener EventTypes.load loadListener false et
    getResult blob fr
    pure mempty

-- | https://github.com/nwolverson/purescript-dom-filereader
readAsText :: Blob -> Aff String
readAsText = readAs readString FileReader.readAsText
