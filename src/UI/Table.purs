-- File Name: Table.purs
-- Description: Table Effects
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 29 Mar 2023 17:34:17
-- Last Modified: 02 May 2023 23:11:42

module UI.Table where

import Prelude

import Data.Array (range)
import Data.Foldable (for_)
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Console (log)
import Grader.Calc (round2dec)
import Grader.Csv (CsvOut)
import Web.DOM.Document as D
import Web.DOM.HTMLCollection as C
import Web.DOM.Internal.Types (Element)
import Web.DOM.Node (setTextContent)
import Web.DOM.NonElementParentNode (getElementById)
import Web.HTML (window)
import Web.HTML.HTMLDocument (toDocument)
import Web.HTML.HTMLElement as HE
import Web.HTML.HTMLTableElement as T
import Web.HTML.HTMLTableRowElement as TR
import Web.HTML.HTMLTableSectionElement as TSE
import Web.HTML.Window (document)

-- | getElementById wrapper
getElem :: D.Document -> String -> Effect (Maybe Element)
getElem doc id = getElementById id $ D.toNonElementParentNode $ doc

-- | creates table cell
createCell :: HE.HTMLElement -> Effect (Maybe HE.HTMLElement)
createCell elem = do
  case row of
    Nothing -> pure Nothing
    Just r -> map Just (TR.insertCell r)
  where
  row = TR.fromHTMLElement elem

-- | sets cell content
setCell :: Maybe HE.HTMLElement -> String -> Effect Unit
setCell elem content =
  case elem of
    Nothing -> log "Couldnt find cell in: 'setCell'"
    Just cell -> do
      setTextContent content node
      where
      node = HE.toNode cell

-- | sets header
setHeader :: T.HTMLTableElement -> Effect Unit
setHeader table = do
  head <- T.createTHead table
  let section = TSE.fromHTMLElement head

  case section of
    Nothing -> pure unit
    Just s -> do
      row <- TSE.insertRow s
      let row_entries = [ "Name", "Points", "Grade", "GradeStr" ]
      for_ row_entries \e -> do
        cell <- createCell row
        setCell cell e

-- | clears all rows of the table
clearTable :: T.HTMLTableElement -> Effect Unit
clearTable table = do
  collection <- T.rows table
  len <- C.length collection
  case len of
    0 -> pure unit
    _ -> do
      for_ (range 0 (len - 1)) \_ ->
        T.deleteRow 0 table

-- | updates the table
setTable :: String -> Array CsvOut -> Effect Unit
setTable id grades = do
  w <- window
  d <- document w
  let doc = toDocument d
  table <- getElem doc id
  case table of
    Nothing -> log "Invalid element in: 'setTable'"
    Just element -> do
      case T.fromElement element of
        Nothing -> log "Invalid element in: 'setTable'"
        Just t -> do
          clearTable t
          setHeader t

          for_ grades \r -> do
            row <- T.insertRow t
            let row_entries = [ r.name, (show r.p), (show $ round2dec $ r.g_num), r.g_str ]

            for_ row_entries \e -> do
              cell <- createCell row
              setCell cell e
