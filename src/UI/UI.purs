-- File Name: Effect.purs
-- Description: Effects
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 27 Mar 2023 22:11:06
-- Last Modified: 02 May 2023 23:11:35

module UI
  ( UpdateTarget
  , disableFields
  , graphUpdate
  , ids
  , outputUpdate
  , run
  , tableUpdate
  ) where

import Prelude

import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Data.Tuple (fst, snd, Tuple)
import Effect (Effect)
import Effect.Aff (Aff, launchAff_, makeAff)
import Effect.Class (liftEffect)
import Effect.Console (log)
import Grader.Calc (Options, generateLinear, generateNonLinear, graphLinear, graphNonLinear)
import Grader.Csv (parseCsv, toCsv, CsvOut)
import UI.File (getFile, readAsText)
import UI.Table (getElem, setTable)
import Web.DOM.Document as D
import Web.DOM.Element as E
import Web.DOM.Internal.Types (Element)
import Web.File.File (toBlob)
import Web.HTML (window)
import Web.HTML.HTMLDocument (toDocument)
import Web.HTML.HTMLElement as HE
import Web.HTML.HTMLInputElement as I
import Web.HTML.Window (document)

foreign import _encodeURIComponent :: String -> String
foreign import _chart :: Array Number -> Array Number -> Options -> String -> Void

-- | update target
data UpdateTarget = Table | Graph | Output

tableUpdate :: UpdateTarget
tableUpdate = Table

graphUpdate :: UpdateTarget
graphUpdate = Graph

outputUpdate :: UpdateTarget
outputUpdate = Output

-- | HTML ids
type Ids =
  { table :: String
  , download_button :: String
  , file_selector :: String
  , lower_input :: String
  , upper_input :: String
  , floor_input :: String
  , ceil_input :: String
  , max_input :: String
  , percent_input :: String
  , positive_input :: String
  , linear_check :: String
  , canvas_container :: String
  , canvas :: String
  }

ids :: Ids
ids =
  { table: "grader-table"
  , download_button: "grader-download"
  , file_selector: "grader-file-selector"
  , lower_input: "grader-lower"
  , upper_input: "grader-upper"
  , floor_input: "grader-floor"
  , ceil_input: "grader-ceil"
  , max_input: "grader-max"
  , percent_input: "grader-percent"
  , positive_input: "grader-positive"
  , linear_check: "grader-linear"
  , canvas_container: "grader-canvas-container"
  , canvas: "grader-canvas"
  }

-- | returns options
getOpts :: Effect (Maybe Options)
getOpts = do
  w <- window
  d <- document w
  let doc = toDocument d
  lower_input <- getElem doc ids.lower_input
  upper_input <- getElem doc ids.upper_input
  floor_input <- getElem doc ids.floor_input
  ceil_input <- getElem doc ids.ceil_input
  max_input <- getElem doc ids.max_input
  percent_input <- getElem doc ids.percent_input
  positive_input <- getElem doc ids.positive_input
  linear_checkbox <- getElem doc ids.linear_check

  lower <- inputValueNum lower_input
  upper <- inputValueNum upper_input
  floor <- inputValueNum floor_input
  ceil <- inputValueNum ceil_input
  max_p <- inputValueNum max_input
  perc <- inputValueNum percent_input
  pos <- inputValueNum positive_input
  lin <- isChecked linear_checkbox

  case { lower: lower, upper: upper, floor: floor, ceil: ceil, max_p: max_p, perc: perc, pos: pos } of
    { lower: Just low, upper: Just up, floor: Just flo, ceil: Just cei, max_p: Just mp, perc: Just per, pos: Just po } ->
      pure $ Just
        { lower: low
        , upper: up
        , floor: flo
        , ceil: cei
        , max_p: mp
        , perc: per
        , pos: po
        , lin: lin
        }
    _ -> pure Nothing

-- | returns the numeric value of an input element
inputValueNum :: Maybe Element -> Effect (Maybe Number)
inputValueNum elem =
  case elem of
    Nothing -> pure Nothing
    Just element ->
      case I.fromElement element of
        Nothing -> pure Nothing
        Just input_element -> map Just (I.valueAsNumber input_element)

-- | disables an input element
disableInput :: Maybe Element -> Boolean -> Effect Unit
disableInput elem b =
  case elem of
    Nothing -> log "Invalid element in: 'disableInput'"
    Just element ->
      case I.fromElement element of
        Nothing -> log "Invalid element in: 'disableInput'"
        Just input -> I.setDisabled b input

-- | checks if an input is checked
isChecked :: Maybe Element -> Effect Boolean
isChecked elem =
  case elem of
    Nothing -> pure false
    Just element ->
      case I.fromElement element of
        Nothing -> pure false
        Just input -> I.checked input

-- | disables input fields on click
disableFields :: Effect Unit
disableFields = do
  w <- window
  d <- document w
  let doc = toDocument d
  linear_checkbox <- getElem doc ids.linear_check
  positive_input <- getElem doc ids.positive_input
  percent_input <- getElem doc ids.percent_input
  c <- isChecked linear_checkbox

  case c of
    true -> do
      disableInput positive_input true
      disableInput percent_input true
    false -> do
      disableInput positive_input false
      disableInput percent_input false

-- | updates the table or the chart
getTable :: String -> Options -> Effect (Maybe (Array CsvOut))
getTable str opts = do
  let input = parseCsv str
  case opts.lin of
    false -> pure $ Just (generateNonLinear input opts)
    true -> pure $ Just (generateLinear input opts)

-- | updates the table or the chart
getGraph :: Options -> Effect (Maybe (Tuple (Array Number) (Array Number)))
getGraph opts = do
  case opts.lin of
    false -> pure $ Just (graphNonLinear opts)
    true -> pure $ Just (graphLinear opts)

-- | creates download link
createDownload :: String -> Effect Unit
createDownload str = do
  w <- window
  d <- document w
  elem <- D.createElement "a" (toDocument d)
  E.setAttribute "href" ("data:text/plain;charset=utf-8," <> (_encodeURIComponent $ str)) elem
  E.setAttribute "download" "output.csv" elem
  case HE.fromElement elem of
    Just e -> HE.click e
    Nothing -> log "Invalid download button in: 'createDownload'"

-- | downloads the file as a csv
download :: String -> Options -> Effect Unit
download str opts = do
  case opts.lin of
    false -> do
      grades <- getTable str opts
      case grades of
        Nothing -> log "Couldn't download csv in: 'download'"
        Just g -> createDownload (toCsv g)
    true -> do
      grades <- getTable str opts
      case grades of
        Nothing -> log "Couldn't download csv in: 'download'"
        Just g -> createDownload (toCsv g)

-- | run file reader
run :: UpdateTarget -> Aff Unit
run target = liftEffect do
  w <- window
  d <- document w
  let doc = toDocument d
  file_selector <- getElem doc ids.file_selector
  file <- getFile file_selector
  opts <- getOpts

  case opts of
    Nothing -> log "Invalid options"
    Just o ->
      case target of
        Graph -> do
          xydata <- getGraph o
          case xydata of
            Nothing -> log "Nothing"
            Just xy -> do
              let _ = _chart (fst xy) (snd xy) o ids.canvas
              pure unit
        _ -> do
          case file of
            Nothing -> log "Couldn't find any file to read in: 'run'"
            Just f -> launchAff_ do
              s <- readAsText (toBlob f)
              makeAff \done -> do
                _ <- case target of
                  Output -> download s o
                  Table -> do
                    grades <- getTable s o
                    case grades of
                      Nothing -> log "Nothing"
                      Just g -> setTable ids.table g
                  _ -> pure mempty
                _ <- done (Right mempty)
                pure mempty
